const calculator = (a, b, operation) => {
  if (operation === "suma") return a + b;
  else if (operation === "resta") return a - b;
  else if (operation === "multiplicacion") return a * b;
  else if (operation === "division") return a / b;
  else return "error";
};

module.exports = calculator;
