const calculator = require("../calculator");

describe("Test calculator: Suma", () => {
  it("should return 7 with values 2, 5", () => {
    const result = calculator(2, 5, "suma");
    expect(result).toEqual(7);
  });
  it("should return 7 with values 5, 2", () => {
    const result = calculator(5, 2, "suma");
    expect(result).toEqual(7);
  });
  it("should return 0 with values 0, 0", () => {
    const result = calculator(0, 0, "suma");
    expect(result).toEqual(0);
  });
});

describe("Test calculator: Resta", () => {
  it("should return 1 with values 10, 9", () => {
    const result = calculator(10, 9, "resta");
    expect(result).toEqual(1);
  });
  it("should return -1 with values 9, 10", () => {
    const result = calculator(9, 10, "resta");
    expect(result).toEqual(-1);
  });
  it("should return 0 with values 1, 1", () => {
    const result = calculator(1, 1, "resta");
    expect(result).toEqual(0);
  });
  it("should return -10 with values -20, -10", () => {
    const result = calculator(-20, -10, "resta");
    expect(result).toEqual(-10);
  });
});

describe("Test calculator: Multiplicacion", () => {
  it("should return 21 with values 7, 3", () => {
    const result = calculator(7, 3, "multiplicacion");
    expect(result).toEqual(21);
  });
  it("should return 21 with values 3, 7", () => {
    const result = calculator(3, 7, "multiplicacion");
    expect(result).toEqual(21);
  });
  it("should return 2 with values 1, 2", () => {
    const result = calculator(1, 2, "multiplicacion");
    expect(result).toEqual(2);
  });
  it("should return 0 with values 15, 0", () => {
    const result = calculator(15, 0, "multiplicacion");
    expect(result).toEqual(0);
  });
  it("should return -20 with values -20, -10", () => {
    const result = calculator(-2, 10, "multiplicacion");
    expect(result).toEqual(-20);
  });
});
describe("Test calculator: Division", () => {
  it("should return 21 with values 7, 3", () => {
    const result = calculator(21, 3, "division");
    expect(result).toEqual(7);
  });
  it("should return 21 with values 3, 7", () => {
    const result = calculator(5, 1, "division");
    expect(result).toEqual(5);
  });
  it("should return 2 with values 1, 2", () => {
    const result = calculator(5, 5, "division");
    expect(result).toEqual(1);
  });
  it("should return 2 with values 5, -5", () => {
    const result = calculator(5, -5, "division");
    expect(result).toEqual(-1);
  });
  it("should return 0 with values 0, 5", () => {
    const result = calculator(0, 5, "division");
    expect(result).toEqual(0);
  });
  it("should return Infinity with values 15, 0", () => {
    const result = calculator(15, 0, "division");
    expect(result).toBe(Infinity);
  });
});
describe("Test calculator bad format and negatives", () => {
  it("should return an error message with values 1 and 'suma'", () => {
    const result = calculator(1, "suma");
    expect(result).toBe("error");
  });

  it("should return an error with no values", () => {
    const result = calculator();
    expect(result).toBe("error");
  });

  it("should return an error with an unimplemented operation", () => {
    const result = calculator(5, 5, "raiz");
    expect(result).toBe("error");
  });
});
